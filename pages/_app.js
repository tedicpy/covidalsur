import '../styles/globals.css'
import withLayout from '../components/withLayout.hoc'
import { ToastProvider } from 'react-toast-notifications'
import { AnimateSharedLayout } from "framer-motion"

export function App({ Component, pageProps }) {
  return <>
    <AnimateSharedLayout>
      <ToastProvider
        autoDismiss
        autoDismissTimeout={5000}
        placement="bottom-center"
      >
        <Component {...pageProps} />
      </ToastProvider>
    </AnimateSharedLayout>
  </>
}

export default withLayout(App)