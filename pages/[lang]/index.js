import React, { useState } from 'react'
import { motion } from "framer-motion"
import Layout from '../../components/layout'
import Header from '../../components/header'
import CountrySelector from '../../components/countrySelector'
import DownloadDataSource from '../../components/downloadDataSource'
import ShowVisualizations from '../../components/showVisualizations'
import Footer from '../../components/footer'
import { getCountriesData } from '../../lib/countries'
import withLocale from '../../lib/translations/withLocale.hoc'
import config from '../../config'
import { readLocaleStrings } from '../../lib/translations/readLocaleStrings'
import useTranslation from '../../lib/translations/useTranslation.hook'
import dynamic from 'next/dynamic'

export function Home({ allCountriesData }) {
  const { t, locale } = useTranslation()
  const [ showIntro, showIntroSet ] = useState(false);
  
  const IntroWithNoSSR = dynamic(() => import('../../components/intro'), {
    ssr: false
  })

  return (
    showIntro ? (
      <IntroWithNoSSR showIntroSet={showIntroSet} />
    ) : (
      <Layout home>
        <Header id="header"></Header>
        <motion.section id="country-selector"
          initial={{ opacity: 0 }}
          animate={{ opacity: 1 }}
          transition={{ delay: 0.2 }}
        >
          <CountrySelector id="country-selector" allCountriesData={allCountriesData} ></CountrySelector>
          <div className="mb-20 md:mb-28 xl:mb-36 2xl:mb-44">
            <DownloadDataSource></DownloadDataSource>
            <ShowVisualizations showIntroSet={showIntroSet}></ShowVisualizations>
          </div>
        </motion.section>
        <Footer id="footer"></Footer>
      </Layout>
    )
  )
}

export async function getStaticPaths () {
  const paths = []
  config.locales.forEach(locale => {
    paths.push({ params: { lang: locale } })
  })
  return {
    paths, fallback: false
  }
}

export async function getStaticProps ( props ) {
  let allCountriesData = await getCountriesData({ lang: props.params.lang })
  return {
    props: {
      locale: props.params.lang,
      localeStrings: readLocaleStrings(props.params.lang, ['common', 'home']),
      allCountriesData
    }
  }
}

export default withLocale(Home)