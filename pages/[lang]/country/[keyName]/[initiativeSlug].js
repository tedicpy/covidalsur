import { useState, useEffect, useRef } from 'react'
import { motion } from "framer-motion"
import Layout from '../../../../components/layout'
import Header from '../../../../components/header'
import Footer from '../../../../components/footer'
import QuestionsIndex from '../../../../components/questionsIndex'
import AnswersByCountry from '../../../../components/answersByCountry'
import CountryHeader from '../../../../components/countryHeader'
import { getQuestionsDef, getCountriesData, findCountryDataByKeyNameAndInitiative, getCountryAnswersByKeyNameAndInitiativeSlug } from '../../../../lib/countries'
import withLocale from '../../../../lib/translations/withLocale.hoc'
import config from '../../../../config'
import { readLocaleStrings } from '../../../../lib/translations/readLocaleStrings'
import groupByCountry from '../../../../lib/groupByCountry'

export function Country({ countryData, questionsDef, countryAnswers }) {
  // sticky index
  const [isStickyIndex, setIsStickyIndex] = useState(false)
  const [isFooterInViewport, setIsFooterInViewport] = useState(false)
  const indexRef = useRef(null)
  const footerRef = useRef(null)
  const topOffset = 56 // menu height 3.5rem
  const handleScroll = () => {
    if (indexRef && indexRef.current && window) {
      setIsFooterInViewport(footerRef.current.getBoundingClientRect().top <= window.innerHeight)      
      setIsStickyIndex(indexRef.current.getBoundingClientRect().top <= topOffset)
    }
  }
  useEffect(() => {
    window.addEventListener('scroll', handleScroll)
    window.addEventListener('touchend', handleScroll)
    return () => {
      window.removeEventListener('scroll', () => handleScroll)
      window.removeEventListener('touchend', () => handleScroll)
    }
  }, [])

  return (
    <Layout title={countryData.name} navbar={{ countryData, questionsDef }}>
      <Header id="header"></Header>

      <section className="flex flex-row">
        <div className="relative hidden lg:flex lg:w-4/12 xl:w-3/12">
          <QuestionsIndex isStickyIndex={isStickyIndex} isFooterInViewport={isFooterInViewport} countryData={countryData} questionsDef={questionsDef}></QuestionsIndex>
        </div>

        <div className="flex flex-col w-full lg:w-8/12 xl:w-9/12" ref={indexRef}>
          <CountryHeader countryData={countryData}></CountryHeader>
          <AnswersByCountry countryData={countryData} countryAnswers={countryAnswers} questionsDef={questionsDef}></AnswersByCountry>
        </div>
      </section>
      
      <Footer footerRef={footerRef}></Footer>
    </Layout>
  )
}

export async function getStaticPaths() {
  const paths = []
  for (const locale of config.locales) {
    const countriesData = await getCountriesData({ lang: locale })
    const groupedArray = groupByCountry(countriesData)
    groupedArray.forEach(country => {
      country.forEach(countryData => {
        paths.push({ params: { lang: locale, ...countryData } })
      })
    })
  }
  return {
    paths,
    fallback: false
  }
}


export async function getStaticProps(props) {
  let { keyName, initiativeSlug, lang } = props.params
  const questionsDef = await getQuestionsDef({ lang })
  const countryData = await findCountryDataByKeyNameAndInitiative({ keyName, initiativeSlug, lang })
  const countryAnswers = await getCountryAnswersByKeyNameAndInitiativeSlug({ keyName, initiativeSlug, lang })
  // agrega al objeto de datos básicos la lista de las demás iniciativas del país
  const countriesData = await getCountriesData({ lang })
  const countryInitiatives = countriesData.filter(country => country.keyName === keyName)
  countryData.initiatives = countryInitiatives.map(({ initiativeName, initiativeSlug }) => ({ initiativeName, initiativeSlug }))
  return {
    props: {
      locale: props.params.lang,
      localeStrings: readLocaleStrings(props.params.lang, ['common', 'country']),
      countryData,
      questionsDef,
      countryAnswers
    }
  }
}

export default withLocale(Country)