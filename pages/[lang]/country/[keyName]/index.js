import React from 'react'
import Head from 'next/head'
import { useRouter } from 'next/router'
import { getCountriesData } from '../../../../lib/countries'
import groupByCountry from '../../../../lib/groupByCountry'
import config from '../../../../config'

export default function Index({ allCountriesData }) {
  const router = useRouter()
  React.useEffect(() => {
    let { keyName, lang } = router.query
    let firstInitiativeByKeyName = allCountriesData.find(country => {
      if (country.keyName === keyName) {
        return country
      }
    })
    router.push(router.asPath + firstInitiativeByKeyName.initiativeSlug)
  })
  return (
    <Head>
      <meta name="robots" content="noindex, nofollow" />
    </Head>
  )
}

export async function getStaticPaths() {
  const paths = []
  for (const locale of config.locales) {
    const countriesData = await getCountriesData({ lang: locale })
    const groupedArray = groupByCountry(countriesData)
    groupedArray.forEach(country => {
      paths.push({ params: { lang: locale, keyName: country[0].keyName } })
    })
  }
  return {
    paths,
    fallback: false
  }
}


export async function getStaticProps(props) {
  let { lang } = props.params
  const allCountriesData = await getCountriesData({ lang })
  return {
    props: {
      allCountriesData
    }
  }
}