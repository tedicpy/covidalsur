import Layout from '../components/layout'
import Header from '../components/header'
import Footer from '../components/footer'
import withLocale from '../lib/translations/withLocale.hoc'
import { readLocaleStrings } from '../lib/translations/readLocaleStrings'
import useTranslation from '../lib/translations/useTranslation.hook'
import config from '../config'

export function Custom404() {
  const { t } = useTranslation()
  return (
    <Layout title='404'>
      <Header id="header"></Header>

      <section>
        <div className="p-10 items-top">
          <h2>{t('errors.404')}</h2>
        </div>
      </section>

      <Footer id="footer"></Footer>
    </Layout>
  )
}

export async function getStaticProps(props) {
  return {
    props: {
      locale: config.defaultLocale,
      localeStrings: readLocaleStrings(config.defaultLocale, ['common', 'errors'])
    }
  }
}

export default withLocale(Custom404)