# Descripción

Repositorio para el Observatorio Covid Alsur

El Observatorio Al Sur de Tecnologías de Vigilancia y Pandemias es una iniciativa conjunta de las organizaciones miembros del consorcio Al Sur que busca analizar y sistematizar las medidas gubernamentales (incluidas las asociaciones público-privadas) relacionadas con la implementación de tecnologías de vigilancia y recolección de datos en el contexto de COVID-19 y que podrían impactar en los derechos humanos de las personas.

Más información: https://covid.alsur.lat.

## Desarrollo

Esta herramienta ha sido desarrollada en conjunto con [WebLab](https://www.weblab.com.py/).

# Documentación

El proyecto está basado en [Next.js](https://nextjs.org/), utiliza [Yarn](https://classic.yarnpkg.com/en/) como gestor de dependencias y requiere de [Node.js 10.13](https://nodejs.org/en/) o superior.

Instalar las dependencias ejecutando
```
yarn install
``` 

Para ejecutar el proyecto con el entorno de desarrollo y acceder desde http://localhost:3000 utilizar:
```
yarn dev
```
ó
```
npm run dev
```

Para generar los archivos estáticos utilizar:
```
yarn export
```
ó
```
npm run export
```

## Fuente de datos
Los datos se extraen directamente de los archivos ``.xlsx`` ubicados en la carpeta ``/data`` habiendo para cada idioma un archivo independiente con el código del idioma como sufijo.
Ejemplo:
```
/data
 ├ covid19-en.xlsx
 ├ covid19-es.xlsx (idioma predeterminado)
 └ covid19-pt.xlsx
```
> El órden de los países en las columnas de los archivos ``.xlsx`` de cada idioma deben ser simétricos.

> La segunda fila del ``.xlsx`` corresponde al código [ISO_3166-1 alfa-2](https://es.wikipedia.org/wiki/ISO_3166-1#C.C3.B3digos_oficialmente_asignados) de cada país utilizado para obtener la bandera correcta a partir de la colleción de banderas en svg [flag-icons](https://flagicons.lipis.dev/).

## Configuraciones
En el archivo ``/config.js`` se especifican los parámetros necesarios para obtener los datos a partir de los archivos ``.xlsx`` como el nombre utilizado como prefijo y las columnas y filas de referencia. Además se especifican los idiomas disponibles y el idioma predeterminado.

## Traducciones
En la carpeta ``/locales`` se encuentran los archivos de traducción de las diferentes secciones y componentes agrupadas por idioma.

El idioma predeterminado es ``en``.

Ejemplo:
```
/locales
 ├ en (idioma predeterminado)
 ├ es
 | ├ common.js (utilizado principalmente por el menú)
 | ├ ...
 | └ home.js (utilizado en la portada)
 └ pt
```

> El sistema de internacionalización i18n es una adaptación de https://medium.com/swlh/how-to-build-a-multilingual-website-in-next-js-2924eeb462bc

> En las rutas para hacer referencia a los países se utilizan los nombres de cada país definidos con el idioma predeterminado en el archivo ``covid19-en.xlsx``.

## Agregar nuevo idioma

Para agregar un nuevo idioma se deben seguir los siguientes pasos:
- Crear en la carpeta ``/locales`` una carpeta con el código del nuevo idioma.
- Incluir en la carpeta creada los archivos de traducción correspondientes (``common.js, ..., home.js``), se puede agilizar el proceso duplicando la carpeta del idioma predeterminado ``en``.
- Agregar el código del nuevo idioma en el parámetro ``locales`` del archivo de configuraciones ``/config.js``.
- Agregar el nombre del nuevo idioma en el parámetro ``locale-select`` en el archivo de traducción ``common.js`` para cada idioma disponible.
- Agregar la traducción de las rutas al nuevo idioma en el archivo ``/locales/en/pages.js``

## Páginas estáticas

Para agregar nuevas páginas estáticas:
- Agregar el contenido en sus diferentes traducciones en las subcarpetas de ``/locales`` utilizando como nombre de archivo la ruta en el idioma predeterminado (Si sólo se cuenta con un idioma agregar el archivo en la carpeta del idioma predeterminado ``/locales/en/[my-new-page].js`` de esta manera se mostrará el mismo contenido para todos los idiomas).
- Agregar el nombre traducido de la nueva página en la lista ``pages`` del archivo ``common.js`` de cada idioma.
- Agregar las traducciones de la ruta en el archivo ``/locales/en/pages.js``
- Agregar la ruta con el idioma predeterminado dentro de la función ``getStaticPaths`` del archivo ``/pages/[lang]/page/[page].js`` .

## Estructura de datos en celdas
- Eliminar hipervínculos del contenido de las celdas para evitar conflictos de interpretación.

- En las celdas se puede utilizar el metalenguaje [Markdown](https://www.markdownguide.org/basic-syntax/) para agregar formato al texto.

- Si en la celda no se encuentran datos válidos se presenta la respuesta como texto.

- Si se desea mostrar valores numéricos o porcentuales en forma de gráfico, es necesario que los valores estén en forma de ``ETIQUETA: VALOR``, por ejemplo: ``Población con acceso a internet: 53,7%``. 
  > Si se desea mostrar otros valores porcentuales sin representación gráfica es necesario utilizar la palabra ``porciento`` en vez del símbolo ``%`` para evitar conflictos de interpretación.

  > Evitar el uso del símbolo ``:`` en los tipos de dato con gráficos debido a posibles conflictos al extraer las etiquetas y los valores.

- Para la extracción de datos al generar los gráficos estadísticos es necesario el siguiente formato:
  
  - Para cada dato separar el nombre y el valor mediante ``:`` (dos puntos). Ejemplo:
    ```
    Etiqueta 1: 20,5%
    ```

  - Agregar al final de cada dato ``↵`` (enter). Ejemplo:
    ```
    Etiqueta 1: 20,5%
    Etiqueta 2: 70,8%
    ```
    > Para introducir `` ↵`` (enter) dentro de una celda utilizar la combinación de teclas: ``alt + ↵ (enter)``.

  - Para cada valor porcentual o numérico debe haber una etiqueta.

- Existen los siguientes tipos de datos:

  - **chart_pie_single_percentage**: Tipo de dato para graficar un gráfico de torta a partir de un único valor porcentual. Ejemplo: 
    ```
    Población con acceso a internet: 53,7%.
    Fuente: Dirección General de Estadísticas, Encuestas y Censo (Dgeec), 2019
    ```
  - **chart_bars_percentage**: Tipo de dato para graficar gráficos de barra a partir de múltiples valores porcentuales. Ejemplo: 
    ```
    En 2019, entre los hogares con acceso a internet (46.5 millones), contaban con 
    Banda ancha fija: 61%.
    Conexiones móviles: 27%. 
    Fuente: Cetic.br, 2020, https://cetic.br/pt/tics/domicilios/2019/individuos/C2A/
    ```
  - **chart_pie_number**: Tipo de dato para graficar un gráfico de torta a partir de múltiples valores numéricos. Ejemplo: 
    ```
    Según el Ministerio de Salud, 5.473 personas que descargaron la aplicación, de las cuales son
    Hombres: 2.484.
    Mujeres: 2.989.
    Fuente: Ministerio de Salud, 2020, https://www.mspbs.gov.py/reporte-covid19.html y consultas presenciales.
    ```
  - **text**: Tipo de dato de texto. Ejemplo: 
    ```
    Ministerio de Salud Pública y Bienestar Social
    ```
  - **yes_no_text**: Tipo de dato de texto que incluye un ícono relativo a la primera palabra contenida en el texto, que puede ser: Si ó No. Ejemplo: 
    ```
    Si. Es necesario enviar un correo electrónico a coronapp@minsal.cl.
    ```
  - **date**: Tipo de dato de fecha, muestra fechas especificando el día de la semana y el mes en letras. Requiere el formato: ``DD/MM/YYYY``. Ejemplo:
    ```
    22/3/2020
    ```


- Ejemplos con problemas de formato:
  
  Faltan ``:`` (dos puntos) y `` ↵`` (enter)
  ```
  Hogares con teléfono móvil 96,7% Hogares con conexión a Internet (wifi, o modem) 28,3% Fuente: Dgeec, 2019, https://www.dgeec.gov.py/microdatos/Encuesta-Permanente-de-Hogares-Continua.php y https://www.dgeec.gov.py/publication-single.php?codec=NjU=.
  ```

  Se utiliza el símbolo ``%`` en vez de la palabra ``porciento`` en valores que no deben ser incluidos en el gráfico.
  ```
  Conexiones de banda ancha fija: 7,9%.
  Conexiones móviles: 91,9%.
  El Estado Plurinacional de Bolivia registra 10.407.690 conexiones a Internet al primer semestre 2020. Dentro de las conexiones a internet, 7,9% correspondían a banda ancha fija y 91,9% a conexiones móviles.
  ```
  
  Falta etiqueta correspondiente al valor porcentual:
  ```
  53,7% de la población cuenta con acceso a internet. Fuente: Dirección General de Estadísticas, Encuestas y Censo (Dgeec), 2019, https://www.dgeec.gov.py/microdatos/Encuesta-Permanente-de-Hogares-Continua.php
  ```

  Falta agregar `` ↵`` (enter) al final del dato:
  ```
  Población con acceso a internet: 53,7%. Fuente: Dirección General de Estadísticas, Encuestas y Censo (Dgeec), 2019, https://www.dgeec.gov.py/microdatos/Encuesta-Permanente-de-Hogares-Continua.php
  ```

  Falta agregar `` ↵`` (enter) al final del dato:
  ```
  Según el Ministerio de Salud, 5.473 personas que descargaron la aplicación, de las cuales 2.484 son hombres y 2.989 son mujeres. Fuente: Ministerio de Salud, 2020, https://www.mspbs.gov.py/reporte-covid19.html y consultas presenciales.
  ```

- Ejemplos con formato adecuado:
  ```
  Hogares con teléfono móvil: 96,7%
  Hogares con conexión a Internet (wifi, o modem): 28,3%
  Fuente: Dgeec, 2019, https://www.dgeec.gov.py/microdatos/Encuesta-Permanente-de-Hogares-Continua.php y https://www.dgeec.gov.py/publication-single.php?codec=NjU=.
  ```

  ```
  Conexiones de banda ancha fija:  7,9%.
  Conexiones móviles: 91,9%.
  El Estado Plurinacional de Bolivia registra 10.407.690 conexiones a Internet al primer semestre 2020. Dentro de las conexiones a internet, 7,9 porciento correspondían a banda ancha fija y 91,9 porciento a conexiones móviles.
  ```

  ```
  Población con acceso a internet: 53,7%.
  Fuente: Dirección General de Estadísticas, Encuestas y Censo (Dgeec), 2019, https://www.dgeec.gov.py/microdatos/Encuesta-Permanente-de-Hogares-Continua.php
  ```

  ```
  teléfonos móviles, tabletas, laptops y computadoras de escritorio. Fuente: Dgeec, 2019, https://www.dgeec.gov.py/microdatos/Encuesta-Permanente-de-Hogares-Continua.php y https://www.dgeec.gov.py/publication-single.php?codec=NjU=
  ```

  ```
  Según el Ministerio de Salud, 5.473 personas que descargaron la aplicación, de las cuales son
  Hombres: 2.484.
  Mujeres: 2.989.
  Fuente: Ministerio de Salud, 2020, https://www.mspbs.gov.py/reporte-covid19.html y consultas presenciales.
  ```

## Produción
Utilizar la opción ``trailingSlash: true`` en el archivo ``next.config.js`` con la siguiente configuración de Nginx
```
location / {
  try_files $uri $uri.html $uri/ @rewrite;
}
```