module.exports = {
  // languages
  defaultLocale: 'en',
  locales: ['es', 'en', 'pt'],
  // data extraction
  dataSourcesFolder: 'data',
  dataSourcesPrefix: 'covid19',
  questionColumn: 'C',
  questionCategoryColumn: 'A',
  questionDimensionColumn: 'B',
  firstAnswersColumn: 'F',
  firstQuestionRow: 3,
  countriesNameRow: 5,
  initiativeRow: 3,
  // data representation
  countriesCodeRow: 2,
  dataTypeColumn: 'E'
}
