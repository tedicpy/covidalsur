const colors = require('tailwindcss/colors')

module.exports = {
  purge: ['./components/**/*.{js,ts,jsx,tsx}', './pages/**/*.{js,ts,jsx,tsx}'],
  darkMode: 'media', // 'media' or 'class'
  theme: {
    colors: {
      ...colors,
      brandRed: '#fa384f',
      brandDarkBlue: '#031f5c',
      brandBlue: '#0338d6',
      brandPink: '#fcc7d6',
      brandPalePink: '#fff8fc'
    }
  },
  variants: {
    extend: {},
  },
  plugins: [
    require('@tailwindcss/typography'),
    require('tailwindcss-textshadow')
  ],
}