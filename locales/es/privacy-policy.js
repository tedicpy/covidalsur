export default {
  'content': `
  
## Política de privacidad
Lea esta Política de privacidad de TEDIC de forma completa y cuidadosamente antes de usar www.alsur.lat (el "Sitio") y los proyectos, las características, el contenido o las aplicaciones ofrecidas por la ONG TEDIC. y nuestras publicaciones afiliadas ("nosotros" o "nuestro") (en conjunto con el Sitio , los "Proyectos y servicios"). Este Acuerdo de usuario establece los términos y condiciones legalmente vinculantes para su utilización del Sitio y los Proyectos.

En cumplimiento de lo dispuesto por la Ley Nº 1682/2001, “Que reglamenta la información de carácter privado”, TEDIC desea poner en conocimiento de sus usuarios de este sitio web, los siguientes aspectos relacionados con sus datos personales:

### Identidad y domicilio del titular del banco de datos personales o encargado del tratamiento
El titular del presente banco de datos en el que se almacenarán los datos personales facilitados en la presente solicitud es TEDIC con domicilio en las calles 15 de Agosto 823 y Humaitá, Asunción – Paraguay.

Se informa al usuario que, cualquier tratamiento de datos personales se ajusta a lo establecido por la legislación vigente en Paraguay en la materia.

El sitio web recoge direcciones IP (una dirección de Protocolo de Internet, o dirección IP, es un número identificador único de cada ordenador o dispositivo de red en Internet). Se utilizará su dirección IP para determinar si accede a los servicios desde dentro o desde fuera de la República de Paraguay.

### Finalidad
TEDIC tratará sus datos con la finalidad de asegurar el correcto funcionamiento de este servicio web y aplicación móvil.

Adicionalmente, usted autoriza a TEDIC para usar la información de los registros de información generados en la plataforma como insumo para generar reportes oficiales sobre casos de Torturas en Paraguay y para investigaciones académicas. En ningún caso usaremos tu nombre ni divulgaremos o compartiremos con terceros ninguna información que pueda identificarte.

Así mismo, usted autoriza al TEDIC a usar su correo electrónico para el envío de comunicaciones electrónicas en el futuro relativas a la plataforma de TEDIC.

Sus datos personales sólo serán utilizados con propósitos limitados, tal como los expuestos precedentemente.

### Transferencias y destinatarios
Por la propia naturaleza del servicio web y aplicación móvil, en caso que el reporte sea considerado un caso de tortura, TEDIC transferirá al Poder Judicial la información personal que la persona usuaria coloque en dicho documento para abrir un expediente judicial. No obstante, según la Ley paraguaya, quedarán prohibidas de usar y publicar información sensibles a terceros, para un fin distinto.

Adicionalmente, TEDIC contrata los servicios en la nube de envío de correos electrónicos a través de Mailchimp, The Rocket Science Group, LLC, 675 Ponce de Leon Ave NE, Suite 5000, Atlanta, GA 30308 USA. Por ende, el dato personal correo electrónico podría ser compartido con esta empresa.

### Plazo de conservación
Los datos personales como nombre, número de documento de identidad, domicilio y copia del documento de identidad se conservarán por un período máximo de cinco (5) años. Vencido este plazo, se eliminarán de nuestros servidores y no quedará registro de ellos.

Los datos personales como correo electrónico, y la información sensible recolectada, se conservarán durante un plazo de cinco (5) años o hasta que se solicite la cancelación por el titular del dato.

### Ejercicio de los derechos de información, acceso, rectificación, cancelación y oposición de los datos
Como titular de sus datos personales el usuario tiene el derecho de acceder a sus datos en posesión de TEDIC; conocer las características de su tratamiento, rectificarlos en caso de ser inexactos o incompletos; solicitar sean suprimidos o cancelados al considerarlos innecesarios para las finalidades previamente expuestas o bien oponerse a su tratamiento para fines específicos.

La persona usuaria podrá en todo momento revocar el consentimiento otorgado expresamente, tanto como limitar el uso o divulgación de sus datos personales.

La persona usuaria podrá dirigir su solicitud de ejercicio de los derechos a la siguiente dirección: 15 de agosto 823 casi Humaitá, Asunción – Paraguay., o a la siguiente dirección de correo electrónico: info[@]tedic.org

A fin de ejercer los derechos antes mencionados, la persona usuaria deberá presentar en el domicilio especificado previamente, la solicitud respectiva en los términos los estándares internacionales (incluyendo: nombre del titular del dato personal y domicilio u otro medio para recibir respuesta; documentos que acrediten su identidad o la representación legal; descripción clara y precisa de los datos respecto de los que busca ejercer sus derechos y otros elementos o documentos que faciliten la localización de los datos).

De considerar la persona usuaria que no ha sido atendido en el ejercicio de sus derechos puede presentar una reclamación ante el poder judicial, con la acción de habeas data.

TEDIC será responsable del banco de datos personales contenidos en esta web. Con el objeto de evitar la pérdida, mal uso, alteración, acceso no autorizado y robo de los datos personales o información confidencial facilitados por los usuarios y/o los clientes, TEDIC ha adoptado los niveles de seguridad y de protección de datos personales legalmente requeridos, y ha instalado todos los medios y medidas técnicas a su alcance.

Fecha de efectividad del Acuerdo de usuario y políticas de privacidad: 01/01/2019

  `,
}