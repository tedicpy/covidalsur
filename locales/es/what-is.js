export default {
  // El Observatorio Al Sur de Tecnologías de Vigilancia y Pandemias es una iniciativa conjunta de las organizaciones miembros del consorcio Al Sur que busca analizar y sistematizar las medidas gubernamentales (incluidas las asociaciones público-privadas) relacionadas con la implementación de tecnologías de vigilancia y recolección de datos en el contexto de COVID-19 y que podrían impactar en los derechos humanos de las personas.
  'content': `

Este Observatorio busca compartir información entre organizaciones para comprender mejor cómo esta pandemia podría moldear el futuro panorama local y regional sobre los usos de datos personales y tecnologías de vigilancia. Esto, a su vez, nos permite desarrollar acciones conjuntas para promover y fomentar el respeto de los derechos fundamentales en la implementación de la tecnología digital.

## Los principales objetivos del Observatorio son:

- Mapear las tecnologías digitales implementadas por los gobiernos de América Latina destinadas a recopilar información de la población en el contexto del COVID-19.
- Informar los debates y crear conciencia entre los defensores de los derechos humanos y el público en general sobre el alcance, el impacto y las amenazas potenciales de estas tecnologías de vigilancia y prácticas de recolección de datos para los derechos humanos.
- Comprender las principales tendencias en la región, las protecciones legales en torno a la implementación de acciones por parte de los gobiernos y los desafíos de derechos humanos que prevemos luego de que se enfrente la crisis.

> El Observatorio Al Sur de Tecnologías de Vigilancia y Pandemias ha sido desarrollado gracias a la donación de Open Society Foundations.

  `,
}