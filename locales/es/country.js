export default {
  'index': 'Índice',
  'country': 'País',
  'initiatives': 'Iniciativas',
  'answers': 'Respuestas',
  'copy-link': 'Copiar enlace',
  'link-copied': 'Enlace copiado!',
  'yes': 'si',
  'yes-alt': 'sí',
  'no': 'no',
  'quantity': 'Cantidad',
  'percentage': 'Porcentaje',
  'people': 'personas',
  'remaining': 'Restante'
}