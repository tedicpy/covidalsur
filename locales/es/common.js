export default {
  'site-title': 'Observatorio Covid-19 Al Sur',
  'observatory': 'Observatorio',
  'covid-19': 'Covid-19',
  'al-sur': 'Al Sur',
  'close-intro': 'Cerrar y ver detalles',
  'what-is': { // incluir espacios al inicio de 1, 3, 5, 6 y 7
    '0': 'El Observatorio Al Sur de Tecnologías de Vigilancia y Pandemias',
    '1': ' es una iniciativa conjunta de las organizaciones miembros del consorcio Al Sur',
    '2': 'Busca analizar y sistematizar las medidas gubernamentales',
    '3': ' (incluidas las asociaciones público-privadas) relacionadas con la implementación de tecnologías de vigilancia y recolección de datos',
    '4': 'En el contexto de COVID-19 y',
    '5': ' que podrían impactar en los ',
    '6': ' derechos humanos',
    '7': ' de las personas.'
  },
  'know-more': 'Saber más',
  'flag-of': 'Bandera de',
  'menu' : {
    'home': 'Inicio',
    'what-is': 'Qué es',
    'contacts': 'Contacto'
  },
  'download': {
    '0': 'Descarga',
    '1': 'los resultados de la investigación'
  },
  'showVisualizations': {
    '0': 'Accede',
    '1': 'a visualizaciones sobre estos datos'
  },
  'locale-select': {
    'es': 'Español',
    'pt': 'Portugués',
    'en': 'Inglés'
  },
  'pages': {
    'what-is': 'Qué es',
    'privacy-policy': 'Política de privacidad',
    'terms-and-conditions': 'Términos y Condiciones',
    'contacts': 'Contacto'
  },
	'footer': {
    'by': 'Sitio por',
	  'logo': 'Logo de Tedic'
	}
}
