export default {
  'index': 'Índice',
  'country': 'País',
  'initiatives': 'Iniciativas',
  'answers': 'Respostas',
  'copy-link': 'Copiar link',
  'link-copied': 'Link copiado!',
  'yes': 'sim',
  'yes-alt': 'sim',
  'no': 'não',
  'quantity': 'Quantidade',
  'percentage': 'Porcentagem',
  'people': 'pessoas',
  'remaining': 'Restante'
}