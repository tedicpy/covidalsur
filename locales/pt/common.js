export default {
  'site-title': 'Observatório Covid-19 Al Sur',
  'observatory': 'Observatório',
  'covid-19': 'Covid-19',
  'al-sur': 'Al Sur',
  'close-intro': 'Fechar é ver detalhes',
  'what-is': { // incluir espacios al inicio de 1, 3, 5, 6 y 7
    '0': 'O Observatório Al Sur de Tecnologias de Monitoramento e Pandemias',
    '1': ' é uma iniciativa conjunta das organizações membros do consórcio Al Sur',
    '2': 'Tem como objetivo analisar e sistematizar as medidas governamentais',
    '3': ' (incluídas as asociações público-privadas) relacionadas com a implementação de tecnologias de vigilância e coleta de dados',
    '4': 'No contexto do COVID-19 e',
    '5': ' que poderiam afetar os ',
    '6': ' direitos humanos',
    '7': ' das pessoas.'
  },
  'know-more': 'Saber mais',
  'flag-of': 'Bandeira de',
  'menu' : {
    'home': 'Início',
    'what-is': 'O que é',
    'contacts': 'Contato'
  },
  'download': {
    '0': 'Download',
    '1': 'os resultados da pesquisa.'
  },
  'showVisualizations': {
    '0': 'Acesse',
    '1': 'às visualizações sobre estes dados'
  },
  'locale-select': {
    'es': 'Espanhol',
    'pt': 'Português',
    'en': 'Inglês'
  },
  'pages': {
    'what-is': 'O que é',
    'privacy-policy': 'Política de privacidade',
    'terms-and-conditions': 'Termos e Condições',
    'contacts': 'Contato'
  },
	'footer': {
	  'by': 'Site por',
    'logo': 'Logo de Tedic'
	}
}
