export default {
  'content': `

Este Observatório busca compartilhar informações entre organizações para compreender melhor como essa pandemia poderia moldar o futuro cenário local e regional sobre o uso de dados pessoais e tecnologias de monitoramento. Isso, por sua vez, nos permite desenvolver ações conjuntas para promover e estimular o respeito aos direitos fundamentais na implementação da tecnologia digital.

## Os principais objetivos do Observatório são:

- Mapear as tecnologias digitais implementadas pelos governos da América Latina destinadas a coletar informação da população no contexto do COVID-19.
- Dar subsídios aos debates e criar consciência entre os defensores dos direitos humanos e o público em geral sobre o alcance, o impacto e as potenciais ameaças dessas tecnologias de monitoramento e práticas de coleta de dados aos direitos humanos.
- Compreender as principais tendências na região, as proteções legais em torno da implementação de ações por parte dos governos e os desafios de direitos humanos que prevemos no enfrentamento da crise.

> O Observatório Al Sur de Tecnologias de Monitoramento e Pandemias foi desenvolvido graças à doação da Open Society Foundations.

  `,
}
