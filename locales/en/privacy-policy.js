export default {
  'content': `
  
## Privacy Policy
Please read this TEDIC Privacy Policy completely and carefully before using www.alsur.lat (the "Site") and the projects, features, content or applications offered by the NGO TEDIC. and our affiliated publications ("we" or "our") (in conjunction with the Site, the "Projects and services"). This User Agreement sets forth the legally binding terms and conditions for your use of the Site and the Projects.

In compliance with the provisions of Law No. 1682/2001, "Which regulates private information", TEDIC wishes to inform its users of this website, the following aspects related to their personal data:

### Identity and address of the owner of the personal data bank or data controller
The owner of this data bank in which the personal data provided in this application will be stored is TEDIC with address at streets 15 de Agosto 823 and Humaitá, Asunción - Paraguay.

The user is informed that any personal data processing complies with the provisions of current legislation in Paraguay on the matter.

The website collects IP addresses (an Internet Protocol address, or IP address, is a unique identifier number for every computer or network device on the Internet). Your IP address will be used to determine whether you access the services from within or from outside the Republic of Paraguay.

### Purpose
TEDIC will process your data in order to ensure the proper functioning of this web service and mobile application.

Additionally, you authorize TEDIC to use the information from the information records generated on the platform as input to generate official reports on cases of torture in Paraguay and for academic research. In no case will we use your name or disclose or share with third parties any information that can identify you.

Likewise, you authorize TEDIC to use your email to send electronic communications in the future related to the TEDIC platform.

Your personal data will only be used for limited purposes, such as those set out above.

### Transfers and recipients
Due to the very nature of the web service and mobile application, in the event that the report is considered a case of torture, TEDIC will transfer to the Judiciary the personal information that the user places in said document to open a judicial file. However, according to Paraguayan Law, they will be prohibited from using and publishing sensitive information to third parties, for a different purpose.

Additionally, TEDIC contracts the services in the cloud for sending emails through Mailchimp, The Rocket Science Group, LLC, 675 Ponce de Leon Ave NE, Suite 5000, Atlanta, GA 30308 USA. Therefore, the personal email data could be shared with this company.

### Conservation period
Personal data such as name, identity document number, address and copy of the identity document will be kept for a maximum period of five (5) years. After this period, they will be deleted from our servers and there will be no record of them.

Personal data such as email, and sensitive information collected, will be kept for a period of five (5) years or until the cancellation is requested by the owner of the data.

### Exercise of the rights of information, access, rectification, cancellation and opposition of the data
As the owner of their personal data, the user has the right to access their data held by TEDIC; know the characteristics of your treatment, rectify them if they are inaccurate or incomplete; request that they be deleted or canceled as they are considered unnecessary for the previously stated purposes or oppose their treatment for specific purposes.

The user may at any time revoke the consent expressly granted, as well as limit the use or disclosure of their personal data.

The user may direct their request to exercise their rights to the following address: August 15, 823 almost Humaitá, Asunción - Paraguay., Or to the following email address: info [@] tedic.org

In order to exercise the aforementioned rights, the user must present at the previously specified address, the respective request in the terms of international standards (including: name of the owner of the personal data and address or other means to receive a response; documents that prove your identity or legal representation; clear and precise description of the data with respect to which you seek to exercise your rights and other elements or documents that facilitate the location of the data).

If the user considers that she has not been treated in the exercise of her rights, she can file a claim with the power

  `,
}