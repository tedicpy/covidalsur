export default {
  'paths': [{
    'es': 'que-es',
    'pt': 'que-es',
    'en': 'what-is'
  }, {
    'es': 'politica-de-privacidad',
    'pt': 'politica-de-privacidad',
    'en': 'privacy-policy'
  }, {
    'es': 'terminos-y-condiciones',
    'pt': 'terminos-y-condiciones',
    'en': 'terms-and-conditions'
  }, {
    'es': 'contactos',
    'pt': 'contactos',
    'en': 'contacts'
  }]
}
