export default {
  'content': `
  
## Contacts

Sed massa massa, euismod sit amet imperdiet et, bibendum sit amet quam. Morbi ante ante, gravida ac tortor non, vulputate suscipit sem. Aliquam eleifend vulputate arcu eu volutpat. Maecenas orci massa, egestas sit amet tristique sed, posuere eu neque. Duis finibus massa in diam laoreet porta. Aliquam malesuada leo id malesuada varius. Aenean sed eros pretium leo aliquam dignissim. Morbi euismod ante et lacus aliquet tristique.

- Lorem ipsum dolor sit amet, consectetur adipiscing elit.
- Etiam sed enim vel tellus suscipit sollicitudin.
- Phasellus eu orci tristique, commodo velit non, fermentum metus.
- Morbi et lorem ut massa facilisis rhoncus nec vel dui.
- Duis feugiat dui eu interdum scelerisque.
- Etiam cursus mi id nisi venenatis, ac dignissim augue bibendum.
- Donec fringilla tortor sit amet odio posuere, nec ullamcorper nisl viverra.

  `,
}