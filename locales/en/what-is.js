export default {
  // The Al Sur Observatory on Surveillance Technologies and Pandemics is a joint initiative of the member organizations of the Al Sur consortium. The Observatory seeks to analyze and systematize government measures (including public-private partnerships) related to the implementation of surveillance technologies and data collection in the context of COVID-19, which could impact the human rights of their citizens.
  'content': `

The Observatory seeks to share information between organizations, to better understand how this pandemic could shape the future local and regional landscape regarding the use of personal data and surveillance technologies. This, in turn, allows us to develop joint actions to promote and encourage respect for fundamental rights in the implementation of digital technology.

## The main objectives of the Observatory are:

- Mapping the digital technologies implemented by the governments of Latin America which aim at collecting information from the population in the context of COVID-19.
- Informing debates and raising awareness among human rights defenders and the general public about the scope and impact of these surveillance technologies and data collection practices, and the potential threats to human rights.
- Understanding the regional main trends, the legal protections concerning the implementation of actions by governments and the challenges to human rights that we anticipate once the crisis is in the past.

> The Al Sur Observatory on Surveillance Technologies and Pandemics has been developed thanks to a grant from Open Society Foundations.

  `,
}
