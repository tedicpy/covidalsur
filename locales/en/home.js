export default {
  'country-select-title': 'Select a country',
  'country-select-subtitle': 'to see the measures applied',
}
