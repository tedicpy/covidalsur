export default {
  'index': 'Index',
  'country': 'Country',
  'initiatives': 'Initiatives',
  'answers': 'Responses',
  'copy-link': 'Copy link',
  'link-copied': 'Link copied!',
  'yes': 'yes',
  'yes-alt': 'yes',
  'no': 'no',
  'quantity': 'Quantity',
  'percentage': 'Percentage',
  'people': 'people',
  'remaining': 'Remaining'
}
