export default {
  'site-title': 'Observatory Covid-19 Al Sur',
  'observatory': 'Observatory',
  'covid-19': 'Covid-19',
  'al-sur': 'Al Sur',
  'close-intro': 'Close and see details',
  'what-is': { // incluir espacios al inicio de 1, 3, 5, 6 y 7
    '0': 'The Al Sur Observatory on Surveillance Technologies and Pandemics',
    '1': ' is a joint initiative of the member organizations of the Al Sur consortium',
    '2': 'The Observatory seeks to analyze and systematize government measures',
    '3': ' (including public-private partnerships) related to the implementation of surveillance technologies and data collection',
    '4': 'in the context of COVID-19,',
    '5': ' which could impact',
    '6': ' the human rights',
    '7': ' of their citizens.'
  },
  'know-more': 'Learn more',
  'flag-of': 'Flag of',
  'menu': {
    'home': 'Home',
    'what-is': 'About',
    'contacts': 'Contact'
  },
  'download': {
    '0': 'Download',
    '1': 'the research results'
  },
  'showVisualizations': {
    '0': 'Access',
    '1': 'to visualizarions of this data'
  },
  'locale-select': {
    'es': 'Spanish',
    'pt': 'Portuguese',
    'en': 'English'
  },
  'pages': {
    'what-is': 'About',
    'privacy-policy': 'Privacy Policy',
    'terms-and-conditions': 'Terms and Conditions',
    'contacts': 'Contact'
  },
	'footer': {
	  'by': 'Site by',
    'logo': 'Tedic\'s logo'
	}
}
