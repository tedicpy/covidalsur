var fs = require('fs');
var archiver = require('archiver');
let XLSX = require('xlsx');
var config = require('./config');

var sourceFolder = './data';
var destFolder = `./public/${config.dataSourcesPrefix}.zip`;
var tempFolder = './.tmp';
var convertDestFolder = './.tmp/converted-data';
var destFormat = ['html', 'ods'];

mkdirSync(tempFolder);
mkdirSync(convertDestFolder);
// convert data sources
config.locales.forEach(locale => {
  let workbook = XLSX.readFile(`${sourceFolder}/${config.dataSourcesPrefix}-${locale}.xlsx`);
  destFormat.forEach(format => {
    XLSX.writeFile(workbook, `${convertDestFolder}/${config.dataSourcesPrefix}-${locale}.${format}`, { bookType: format });
  });
});
console.log('Data sources converted to: ' + destFormat.join('+'));

// compress
var output = fs.createWriteStream(destFolder);
var archive = archiver('zip');
// append files from a sub-directory, putting its contents at the root of archive
archive.directory(convertDestFolder, false);
output.on('close', function () {
  console.log('Data has been archived: ' + archive.pointer() + ' total bytes on ' + destFolder);
});
archive.on('error', function (err) {
  throw err;
});
archive.pipe(output);
archive.finalize();

function mkdirSync(path) {
  try {
    fs.mkdirSync(path);
  } catch (e) {
    if (e.code != 'EEXIST') throw e;
  }
}
