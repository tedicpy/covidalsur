import groupBy from 'lodash.groupby'
import values from 'lodash.values'

export default function groupByCountry(allCountriesData) {
  allCountriesData = allCountriesData.sort((a, b) => {
    if (a.name > b.name) {
      return 1
    } else {
      return -1
    }
  })
  let grouped = groupBy(allCountriesData, 'keyName')
  let groupedArray = values(grouped)
  return groupedArray
}