import ExcelJS from 'exceljs'
import slugify from 'slugify'
import config from '../config'

const selectedWorkbooks = {};

export function parseCountryNames( workbook ) {
  const worksheetList = getWorksheets(workbook)
  const worksheet = worksheetList[0]
  const countryNamesListWithRef = []
  const countryNamesList = []
  const countryNamesRow = worksheet.getRow( config.countriesNameRow )
  const firstAnswersColumn = countryNamesRow.getCell( config.firstAnswersColumn )
  const initialColumnId = firstAnswersColumn._column._number
  const countryCodesRow = worksheet.getRow(config.countriesCodeRow)
  const initiativeRow = worksheet.getRow(config.initiativeRow)
  countryNamesRow.eachCell((cell, colNumber) => {
    if (colNumber < initialColumnId) { return }
    let keyName = slugify(sanitize(cell.value), { lower: true, strict: true })
    let countryCode = countryCodesRow.getCell(colNumber).value
    let initiativeName = sanitize(initiativeRow.getCell(colNumber).value)
    let initiativeSlug = slugify(initiativeName, { lower: true, strict: true })
    countryNamesListWithRef.push({
      ref: cell,
      name: sanitize(cell.value),
      keyName,
      code: countryCode,
      initiativeName,
      initiativeSlug
    })
    countryNamesList.push({
      name: sanitize(cell.value),
      keyName,
      code: countryCode,
      initiativeName,
      initiativeSlug
    })
  })
  return { countryNamesList, countryNamesListWithRef }
}

// recorrer preguntas
  // para cada pregunta obtener su
    // categoría, dimensión y tipo de dato
    // agregar al array de preguntas
    // agregar al array serializable de preguntas
export function parseQuestions( workbook ) {
  const worksheetList = getWorksheets(workbook)
  const worksheet = worksheetList[0]  
  const questionList = []
  const questionDef = []
  worksheet.eachRow((row, rowNumber) => {
    if (rowNumber < config.firstQuestionRow) { return }
    // get question
    let question = row.getCell( config.questionColumn )
    // get categories
    let category = row.getCell( config.questionCategoryColumn )
    // get dimension
    let dimension = row.getCell(config.questionDimensionColumn)
    // get data type
    let dataType = row.getCell( config.dataTypeColumn )
    // ref data list
    questionList.push({ question, category, dimension, dataType, row })
    // serializable data list
    questionDef.push({ 
      question: sanitize(question.value),
      category: sanitize(category.value),
      dimension: sanitize(dimension.value),
      dataType: sanitize(dataType.value)
    })
  })
  return { questionList, questionDef }
}

// recorrer preguntas
  // para cada pregunta obtener la respuesta del país correspondiente
export function parseAnswersByCountryRef( countryRef ) {
  const answersColumn = countryRef._column
  const answersList = []
  answersColumn.eachCell((cell, colNumber) => {
    if (colNumber < config.firstQuestionRow) { return }
    answersList.push( sanitize(cell.value) )
  })
  return answersList
}

export function getWorksheets( workbook ) {
  const worksheetList = []
  workbook.eachSheet((worksheet, sheetId) => {
    worksheetList.push(worksheet)
  })
  return worksheetList
}

export async function getWorkbook(filePath) {
  if (!selectedWorkbooks[filePath]) {
    selectedWorkbooks[filePath] = new ExcelJS.Workbook()
    await selectedWorkbooks[filePath].xlsx.readFile(filePath)
  }
  return selectedWorkbooks[filePath]
}

// prevenir errores de serialización
export function sanitize(value) {
  // Date to string
  if (Object.prototype.toString.call(value) === '[object Date]') {
    return value.toUTCString()
  }
  // richText to string
  if (Object.prototype.toString.call(value) === '[object Object]' && value.richText) {
    return parseRichText(value)
  }
  // Number to string
  if (Object.prototype.toString.call(value) === '[object Number]') {
    return value.toString()
  }
  return value
}

export function parseRichText(value) {
  if (Object.prototype.toString.call(value.richText) === '[object Array]') {
    let parsedText = ''
    value.richText.forEach(({ text }) => {
      parsedText += text
    })
    return parsedText
  }
  return value
}