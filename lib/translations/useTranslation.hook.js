import { useContext } from 'react'
import { LocaleContext } from './locale.context'
import config from '../../config'
import get from 'lodash.get'
import { readLocaleStrings } from './readLocaleStrings'

export default function useTranslation() {
  const { strings, locale } = useContext(LocaleContext)

  function t(key) {
    const gettedString = get(strings, `${locale}.${key}`)
    if(!gettedString) {
      console.warn(`Translation '${key}' for locale '${locale}' not found. Using default locale instead`)
    }
    return gettedString || get(strings, `${config.defaultLocale}.${key}`)
  }

  function getLocaledUrl(defaultUrl) {
    const gettedString = findLocaledUrlInString(defaultUrl, locale, strings)
    return gettedString
  }

  function getDefaultUrl(localedUrl) {
    const gettedString = findDefaultUrlInString(localedUrl, locale, strings)
    return gettedString
  }

  function replaceInPath(currentPath, fromLocale, toLocale) {
    // para cada ruta de página en el idioma locale, reemplazar el nombre en e.target.value
    strings[config.defaultLocale].pages.paths.forEach(pagePaths => {
      const regex = new RegExp(`\/(${pagePaths[fromLocale]})`)
      currentPath = currentPath.replace(regex, `/${pagePaths[toLocale]}`)
    })
    return currentPath
  }

  return {
    t,
    getLocaledUrl,
    getDefaultUrl,
    replaceInPath,
    locale
  }
}

export function getLocaledUrlByLocale(defaultUrl, locale) {
  const strings = readLocaleStrings(locale, ['common'])
  const gettedString = findLocaledUrlInString(defaultUrl, locale, strings)
  return gettedString
}

export function getDefaultUrlByLocale(localedUrl, locale) {
  const strings = readLocaleStrings(locale, ['common'])
  const gettedString = findDefaultUrlInString(localedUrl, locale, strings)
  return gettedString
}

function findLocaledUrlInString(defaultUrl, locale, strings) {
  // buscar defaultUrl en strings.pages.paths y retornar strings.pages.paths.{locale}
  const pagesString = getPageStringByAnyURL(defaultUrl, strings)
  const gettedString = pagesString[locale]
  if (!gettedString) {
    console.warn(`Translation for url '${defaultUrl}' in locale '${locale}' not found. Using default locale instead`)
  }
  return gettedString || defaultUrl
}

function findDefaultUrlInString(localedUrl, locale, strings) {
  const pagesString = getPageStringByAnyURL(localedUrl, strings)
  const gettedString = pagesString[config.defaultLocale]
  if (!gettedString) {
    console.warn(`Global url for '${localedUrl}' not found in translation for '${locale}'. Using '${localedUrl}' anyway`)
  }
  return gettedString || localedUrl
}

function getPageStringByAnyURL(targetURL, strings) {
  return strings[config.defaultLocale].pages.paths.find(pagePaths => {
    return config.locales.find(lang => {
      return pagePaths[lang] === targetURL
    })
  })
}