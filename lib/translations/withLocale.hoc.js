import React from 'react'
import Error from 'next/error'
import { getDisplayName } from 'next/dist/next-server/lib/utils'
import { LocaleProvider } from './locale.context'

export default function WithLocale (WrappedPage) {
  const WithLocale = ({ locale, localeStrings, ...pageProps }) => {
    if (!locale) {
      return <Error statusCode={404} />
    }
    return (
      <LocaleProvider lang={locale} localeStrings={localeStrings}>
        <WrappedPage {...pageProps} />
      </LocaleProvider>
    )
  }

  WithLocale.displayName = `withLang(${getDisplayName(WrappedPage)})`

  return WithLocale
}