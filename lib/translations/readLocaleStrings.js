import config from '../../config'

export function readLocaleStrings(lang, stringNameSpaces) {
  if (!stringNameSpaces) {
    throw new Error('NameSpace for locales is missing')
  }
  stringNameSpaces.push('pages') // localized routes
  const namespaces = {}
  stringNameSpaces.forEach(ns => {
    namespaces[lang] = namespaces[lang] || {}
    namespaces[lang][ns] = requireStringsFile(lang, ns)
    if (lang !== config.defaultLocale) {
      namespaces[config.defaultLocale] = namespaces[config.defaultLocale] || {}
      namespaces[config.defaultLocale][ns] = requireStringsFile(config.defaultLocale, ns)
    }
  })
  return namespaces
}

function requireStringsFile(lang, ns) {
  let localeStrings
  try {
    localeStrings = require(`../../locales/${lang}/${ns}.js`).default
  } catch (error) {
    localeStrings = {}
  }
  return localeStrings
}