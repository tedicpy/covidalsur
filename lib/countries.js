import path from 'path'
import config from '../config'
import { getWorkbook, parseCountryNames, parseQuestions, parseAnswersByCountryRef } from './countryParsers'

/**
 * Retorna la definición de las preguntas en el idioma selecto *lang*
 * { question, dimension, dataType }
 * Ejemplo:
 * ({lang: 'en'}) => [
 *    {
 *      question: '¿Con qué nombre se conoce la iniciativa?',
 *      dimension: 'NOMBRE INICIATIVA',
 *      dataType: 'text'
 *    },
 *    ...
 * ]
 */
export async function getQuestionsDef({ lang }) {
  const { questionDef } = parseQuestions( await getWorkbook(resolveDataSourceFilePath(lang)) )
  return questionDef;
}

/**
 * Retorna los datos básicos de la iniciativa del país especificado en el idioma selecto.
 */
export async function findCountryDataByKeyNameAndInitiative({ keyName, initiativeSlug, lang }) {
  const countryNamesData = await getCountriesData({ lang })
  let selectedCountry
  countryNamesData.forEach(country => {
    if (country.keyName === keyName
      && country.initiativeSlug === initiativeSlug) {
      selectedCountry = country
    }
  })
  return selectedCountry
}

/**
 * Retorna un array con las respuestas en el idioma selecto *lang*
 * a partir del keyName basado en el idioma por defecto *defaultLocale*
 * coincidiendo el orden con las preguntas retornadas por *getQuestionsDef*
 * Ejemplo:
 * ({keyName: 'brasil', initiativeSlug: 'coronavirus-sus', lang: 'es'}) => [
 *    0: 'Coronavirus - SUS',
 *    1: 'La aplicación Coronavirus SUS, ofrecida por el Ministerio de Salud de Brasil, ...',
 *    2: 'Brasil',
 *    3: 'N/A',
 *    ...
 * ]
 */
export async function getCountryAnswersByKeyNameAndInitiativeSlug({ keyName, initiativeSlug, lang }) {
  let countryRef
  let countryNamesTargetLocale, countryNamesDefaultLocale
  countryNamesTargetLocale = parseCountryNames(await getWorkbook(resolveDataSourceFilePath(lang))).countryNamesListWithRef
  if (lang === config.defaultLocale) {
    countryNamesDefaultLocale = countryNamesTargetLocale
  } else {
    countryNamesDefaultLocale = parseCountryNames(await getWorkbook(resolveDataSourceFilePath(config.defaultLocale))).countryNamesListWithRef
  }
  // obtener referencia a la celda en el dataSource del idioma selecto
  countryNamesDefaultLocale.map((countryDefaultLocale, index) => {
    if (countryDefaultLocale.keyName === keyName 
      && countryNamesTargetLocale[index].initiativeSlug === initiativeSlug) {
      countryRef = countryNamesTargetLocale[index].ref
    }
  })
  // extrae y retorna las respuestas
  return parseAnswersByCountryRef(countryRef)
}

/**
 * Retorna para cada iniciativa de cada país: 
 * el keyName del país basado en el idioma por defecto *defaultLocale*
 * el nombre del país en el idioma selecto *lang*,
 * el código del país,
 * el nombre de la iniciativa en el idioma selecto *lang*,
 * el slug de la iniciativa en el idioma selecto *lang*,
 * Ejemplo:
 * ({lang: 'en'}) => [{
 *    keyName: 'brasil', // nombre en el dataSource-es.xlsx (idioma predeterminado)
 *    name: 'Brazil',   // nombre en el dataSource-en.xlsx
 *    code: 'br',
 *    initiativeName: 'Coronavirus - SUS',
 *    initiativeSlug: 'coronavirus-sus'
 * }]
 * El órden de los países en las columnas de los archivos xlsx deben ser simétricos
 */
export async function getCountriesData({ lang }) {
  let countryNamesTargetLocale, countryNamesDefaultLocale
  countryNamesTargetLocale = parseCountryNames( await getWorkbook(resolveDataSourceFilePath(lang)) ).countryNamesList
  if (lang === config.defaultLocale) {
    return countryNamesTargetLocale
  }
  countryNamesDefaultLocale = parseCountryNames( await getWorkbook(resolveDataSourceFilePath(config.defaultLocale)) ).countryNamesList
  let countryNames = []
  countryNamesTargetLocale.map((countryTargetLocale, index) => {
    if (countryNamesDefaultLocale[index]) {
      countryNames.push({
        name: countryTargetLocale.name,
        keyName: countryNamesDefaultLocale[index].keyName,
        code: countryTargetLocale.code,
        initiativeName: countryTargetLocale.initiativeName,
        initiativeSlug: countryTargetLocale.initiativeSlug,
      })
    }
  })
  return countryNames
}

function resolveDataSourceFilePath (lang) {
  return path.resolve(
    config.dataSourcesFolder
    , config.dataSourcesPrefix
    + '-'
    + (lang || config.defaultLocale)
    + '.xlsx'
  )
}