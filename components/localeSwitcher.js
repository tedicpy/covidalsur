import React from 'react'
import { useRouter } from 'next/router'
import config from '../config'
import { LocaleContext } from '../lib/translations/locale.context'
import useTranslation from '../lib/translations/useTranslation.hook'

export default function LocaleSwitcher({ selectClassName, optionClassName }) {
  const { t, replaceInPath } = useTranslation()
  const router = useRouter()
  const { locale } = React.useContext(LocaleContext)

  const handleLocaleChange = React.useCallback(
    (e) => {
      const regex = new RegExp(`^/(${config.locales.join('|')})`)
      // replace locale path
      let newPath = router.asPath.replace(regex, `/${e.target.value}`)
      // replace all static page paths
      router.push(router.pathname, replaceInPath(newPath, locale, e.target.value))
    },
    [locale, router]
  )

  return (
    <select value={locale} onChange={handleLocaleChange} className={selectClassName}>
      {config.locales.map(locale => (
        <option key={locale} value={locale} className={optionClassName}>
          {t(`common.locale-select.${locale}`)}
        </option>
      ))}
    </select>
  )
}