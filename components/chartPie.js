import React from 'react'
import { ResponsivePie } from '@nivo/pie'
import round from 'lodash.round'
import useTranslation from '../lib/translations/useTranslation.hook'

export default function ChartPie({ data, type }) {
  const { t } = useTranslation()
  const colors = ['#fcc7d6', '#fa384f', '#031f5c', '#0338d6', '#fcc7d6', '#fff8fc']
  data.forEach((d, index) => {
    d.color = colors[index]
  })
  const calcPercentage = v => {
    const total = data.reduce((accumulator, d) => {
      return d.value + accumulator
    }, 0)
    return round(v*100/total, 2) + ' %'
  }
  const formatNumber = num => {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
  }
  return (
    <ResponsivePie
      data={data}
      theme={{
        "textColor": "#031f5c",
        "fontSize": 18
      }}
      margin={{ top: 10, bottom: 70, left: 0, right: 10 }}
      valueFormat={calcPercentage}
      radialLabel={d => `${d.label}: ${d.data.value}`}
      colors={d => d.data.color}
      borderColor={{ from: 'color' }}
      enableRadialLabels={false}
      tooltip={({ datum }) => {
        return (
          <div className="px-3 py-2 bg-white rounded shadow-sm">
            <div>
              <span className="relative inline-block w-5 h-5 mr-2 top-1" style={{ backgroundColor: datum.color }}></span>
              <b>{datum.data.label}</b>
            </div>
            <ul>
              {type !== 'percentage' ? (
                <li><u>{t('country.quantity')}:</u> {formatNumber(datum.value)} {t('country.people')}</li>
              ) : ''}
              <li><u>{t('country.percentage')}:</u> {datum.formattedValue}</li>
            </ul>
          </div>
        )
      }}
      legends={[
        {
          anchor: 'bottom-left',
          direction: 'column',
          justify: false,
          translateX: 0,
          translateY: 70,
          itemWidth: 0,
          itemHeight: 35,
          itemsSpacing: 0,
          symbolSize: 25,
          symbolShape: 'circle',
          itemDirection: 'left-to-right',
        }
      ]}
    />
  )
}