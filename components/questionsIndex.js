import { useState, useEffect } from 'react'
import { Link as LinkScroll } from 'react-scroll'
import useTranslation from '../lib/translations/useTranslation.hook'
import InitiativeSelect from './initiativeSelect'
import slugify from 'slugify'

export default function QuestionsIndex({ isStickyIndex, isFooterInViewport, countryData, questionsDef }) {
  const { t } = useTranslation()
  // group questions by category
  const questionsByCategory = []
  let lastCategory
  let index = 1
  questionsDef.forEach(questionDef => {
    if (lastCategory !== questionDef.category) {
      lastCategory = questionDef.category
      questionsByCategory.push({ questions: [], category: questionDef.category })
    }
    questionsByCategory[questionsByCategory.length-1].questions.push( questionDef )
  })
  // active the hashSpy after the default anchor link scrolling
  const [isSpyActive, setIsSpyActive] = useState(false)
  useEffect(() => {
    const timer = setTimeout(() => setIsSpyActive(true), 500)
    return () => {
      clearTimeout(timer)
    }
  }, [isSpyActive])

  return (
    <section className={`block shadow-inner 
      ${isStickyIndex ? 'h-screen overflow-y-scroll left-0 pt-14' : ''}
      ${(isStickyIndex && !isFooterInViewport) ? 'fixed top-0 lg:w-4/12 xl:w-3/12' : ''}
      ${(isFooterInViewport) ? 'absolute bottom-0 lg:w-full' : ''}
    `}>
      
      <InitiativeSelect countryData={countryData}></InitiativeSelect>

      <h3 className="px-4 pb-3 text-xl font-bold uppercase pt-7 text-brandDarkBlue">{t('country.index')}</h3>
      <ul className="flex flex-col pb-10 leading-tight items-top">
        {questionsByCategory.map(({ category, questions }, catIndex) => (
          <li className="py-4" key={category}>
            <a className={`px-4 block font-semibold uppercase border-b-4 ${catIndex % 2 > 0 ? 'border-brandDarkBlue' : 'border-brandRed'}`}>{category}</a>

            <ul className="flex flex-col items-top">
              {questions.map(({ question }) => (
                <li className="px-4 py-1.5" key={question}>
                  {isSpyActive ? (
                    <LinkScroll to={slugify(question, {lower: true})} activeClass='bg-yellow-50' delay={300} smooth={true} spy={true} hashSpy={true} duration={800} href={`#${slugify(question, {lower: true})}`}>
                      <span className="font-semibold mr-0.5">{index++}</span>
                      <span className="font-thin">{question}</span>
                    </LinkScroll>
                  ) : (
                    <>
                      <span className="font-semibold mr-0.5">{index++}</span>
                      <span className="font-thin">{question}</span>
                    </>
                  )}
                </li>
              ))}
            </ul>
          </li>
        ))}
      </ul>
    </section>
  )
}