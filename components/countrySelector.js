import { useContext, Fragment } from 'react'
import Link from 'next/link'
import { motion } from "framer-motion"
import useTranslation from '../lib/translations/useTranslation.hook'
import { LayoutContext } from './layout.context'
import groupByCountry from '../lib/groupByCountry'

export default function CountrySelector({ allCountriesData }) {
  const { locale, t } = useTranslation()
  const { setScrollOnInit } = useContext(LayoutContext)
  let grouped = groupByCountry(allCountriesData)

  return (
    <section className="flex flex-col mb-10 md:mb-14 lg:mb-16 xl:mb-24">
      <h2 className="p-20 pb-5 mx-auto leading-3 text-center text-brandDarkBlue sm:pt-28 md:pt-32 lg:pt-40 xl:pt-48 2xl:pt-52">
        <span className="block text-xl font-bold uppercase lg:text-2xl lg:leading-6 xl:text-3xl lx:leading-6">
          {t('home.country-select-title')}
        </span>
        <span className="block font-light lg:text-lg lg:leading-6 xl:text-2xl xl:leading-6">
          {t('home.country-select-subtitle')}
        </span>
      </h2>

      <div className="p-10 mx-auto 2xl:p-20">
        <ul className="flex flex-row flex-wrap place-content-center">
          {grouped.map((country) => {
            let { name, keyName, code, initiativeSlug } = country[0]
            return <Fragment key={keyName}>
              <motion.li layoutId={keyName}
                className="max-w-sm p-3 sm:p-5 md:p-5 lg:p-6 xl:p-7 2xl:p-8 flex-grow-1"
              >
                <Link as={`/${locale}/country/${encodeURIComponent(keyName)}/${encodeURIComponent(initiativeSlug)}`} href={{
                  pathname: '/[lang]/country/[keyName]/[initiativeSlug]',
                  query: { keyName, initiativeSlug, lang: locale }
                }}
                  scroll={false}
                >
                  <motion.a
                    className="cursor-pointer block"
                    onClick={() => setScrollOnInit('country-header')}
                    whileHover={{ scale: 1.2 }}
                  >
                    <motion.img className="w-24 sm:w-28 md:w-32 block shadow-md hover:shadow-2xl rounded-2xl" src={`/flags-icons/1x1/${code}.svg`} alt={`${t('common.flag-of')} ${name}`} />
                    <motion.span layout="position" className="w-24 sm:w-28 md:w-32 block mt-1 text-sm font-semibold uppercase md:text-base xl:text-lg text-center h-16">
                      {name}
                    </motion.span>
                  </motion.a>
                </Link>
              </motion.li>
            </Fragment>
          })}
        </ul>
      </div>
    </section>
  )
}
