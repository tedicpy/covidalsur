import React from 'react'
import { ResponsiveBar } from '@nivo/bar'

export default function ChartBar({ data, opts={} }) {
  const BarComponent = props => {
    return (
      <g transform={`translate(${props.x},${props.y})`}>
        <rect width={'100%'} height={props.height} fill={'#0000001a'} rx={15} /> {/*trail*/}
        <rect width={props.width} height={props.height} fill={props.color} rx={15} /> 
        <text
          x={10}
          y={props.height/2}
          textAnchor="start"
          dominantBaseline="central"
          style={{
            fontWeight: 400,
            fontSize: 15,
          }}
        >
          {props.label}
        </text>
      </g>
    )
  }
  return (
    <ResponsiveBar
      data={data}
      theme={{
        fontSize: 18
      }}
      keys={['value']}
      indexBy='label'
      margin={{top: 0, bottom: 0, left: 0, right: 0}}
      padding={0.2}
      layout='horizontal'
      valueScale={{ type: 'linear' }}
      indexScale={{ type: 'band', round: true }}
      colors={'#fa384f'}
      borderRadius={17}
      borderColor={{ from: 'color' }}
      maxValue={100}
      axisTop={null}
      axisRight={null}
      axisBottom={null}
      axisLeft={null}
      enableGridY={false}
      label={opts.label || (d => `${d.data.label}: ${d.value}`)}
      labelSkipWidth={8}
      labelSkipHeight={5}
      legends={[]}
      animate={true}
      motionStiffness={90}
      motionDamping={15}
      isInteractive={false}
      barComponent={BarComponent}
    />
  )
}