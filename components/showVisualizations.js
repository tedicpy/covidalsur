import React from 'react'
import Link from 'next/link'
import useTranslation from '../lib/translations/useTranslation.hook'
import config from '../config'

export default function ShowVisualizations({ showIntroSet }) {
  const { t } = useTranslation()
  return (
    <>
      <div className={`flex flex-row p-5 mx-5 bg-brandBlue rounded-2xl place-content-center place-items-center md:max-w-md md:mx-auto mt-12`}>
        <div className="">
          <img className="w-20 -ml-1" src="/svg/stats.svg" alt="" />
        </div>
        <div className="ml-8">
          <button type="button" onClick={() => showIntroSet(true)} className="text-left">
            <a className="leading-tight download-link">
              <span className="text-lg font-bold text-white uppercase border-brandBlue">{t('common.showVisualizations.0')}</span>
              <br />
              <span className="border-b-2 text-blue-200 border-brandBlue">{t('common.showVisualizations.1')}</span>
            </a>
          </button>
        </div>
      </div>
      <style jsx>{`
        a.download-link span {
          border-bottom-width: 1px;
        }
        a.download-link:hover span {
          border-bottom: 1px solid white !important;
        }
      `}</style>
    </>
  )
}