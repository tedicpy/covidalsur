import { useContext } from 'react'
import Link from 'next/link'
import useTranslation from '../lib/translations/useTranslation.hook'
import { LayoutContext } from './layout.context'

export default function Header() {
  const { t, getLocaledUrl, locale } = useTranslation()
  const { setScrollOnInit } = useContext(LayoutContext)

  return (
    <header id="header" className="flex flex-col shadow-lg">
      <div className="p-10 mb-16 mt-14 md:mt-16 md:mb-20 lg:mt-20 lg:mb-24 xl:mt-28 xl:mb-32 2xl:mt-40 2xl:mb-44">
        <Link href={`/${locale}`}>
          <h1 className="flex flex-row items-center mx-auto ml-2 cursor-pointer sm:ml-3 md:ml-4 lg:ml-5 xl:ml-6 2xl:ml-7 place-content-center" alt={t('common.site-title')} title={t('common.site-title')}>
            <div className="flex px-2">
              <img className="w-28 sm:w-32 lg:w-36 xl:w-40 2xl:w-48" src="/svg/logo-img.svg"/>
            </div>
            <div className="flex flex-col px-2 text-3xl text-brandDarkBlue sm:text-4xl lg:text-5xl xl:text-6xl 2xl:text-7xl">
              <div className="">{t('common.observatory')}</div>
              <div className="-mt-2 mb-0.5 lg:-mt-1 lg:mb-1.5 uppercase font-bold text-shadow-xl">{t('common.covid-19')}</div>
              <div className="">
                <img className="h-5 lg:h-7 xl:h-8 2xl:h-9" src="/svg/logo-text-blue.svg" />
              </div>
            </div>
          </h1>
        </Link>
      </div>

      <div id="intro" className="w-full mx-auto">
        <div className="w-full overflow-x-hidden">
          <div className="h-10 -mx-20 overflow-hidden sm:-mx-4 sm:h-12 md:-mx-2 md:h-14 lg:-mx-0 lg:h-16 xl:h-20">
            <img className="place-items-center" src="/svg/dot-block.svg" />
          </div>
        </div>

        <div className="flex flex-col text-lg leading-5 text-center md:flex-row lg:text-xl xl:text-2xl lg:leading-5 xl:leading-7">
          <div className="hidden text-white md:flex place-items-center bg-brandRed">
            <div className="py-8 px-14 lg:px-16 lg:py-9 xl:px-20 xl:py-10 2xl:px-28 2xl:py-16">
              <span className="font-semibold">{t('common.what-is.0')}</span>
              <span className="font-thin">{t('common.what-is.1')}</span>
            </div>
          </div>
          <div className="">
            <div className="text-white p-7 sm:px-14 sm:py-8 lg:px-16 lg:py-9 xl:px-20 xl:py-10 2xl:px-28 2xl:py-16 md:hidden bg-brandRed">
              <span className="font-semibold">{t('common.what-is.0')}</span>
              <span className="font-thin">{t('common.what-is.1')}</span>
            </div>
            <div className="text-white p-7 sm:px-14 sm:py-8 lg:px-16 lg:py-9 xl:px-20 xl:py-10 2xl:px-28 2xl:py-16 bg-brandDarkBlue">
              <span className="font-semibold">{t('common.what-is.2')}</span>
              <span className="font-thin">{t('common.what-is.3')}</span>
            </div>
            <div className="p-7 sm:px-14 sm:py-8 lg:px-16 lg:py-9 xl:px-20 xl:py-10 2xl:px-28 2xl:py-16 bg-brandPink text-brandDarkBlue">
              <span className="font-regular">{t('common.what-is.4')}</span>
              <span className="font-bold">{t('common.what-is.5')}</span>
              <span className="font-black">{t('common.what-is.6')}</span>
              <span className="font-bold">{t('common.what-is.7')}</span>
              <Link href={`/${locale}/page/${getLocaledUrl('what-is')}`} scroll={false}>
                <a onClick={() => setScrollOnInit('intro')} className="block h-6 pl-2 mx-auto mt-3 text-sm leading-6 rounded-full bg-brandRed w-28">
                  <span className="text-white">{t('common.know-more')}</span>
                  <img className="float-right h-6" src="/svg/dot-blue.svg" />
                </a>
              </Link>
            </div>
          </div>
        </div>

      </div>
    </header>
  )
}