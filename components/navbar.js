import { useContext } from 'react'
import { useRouter } from 'next/router'
import Link from 'next/link'
import { Link as LinkScroll } from 'react-scroll'
import LocaleSwitcher from './localeSwitcher'
import QuestionsIndex from './questionsIndex'
import useTranslation from '../lib/translations/useTranslation.hook'
import ActiveLink from './activeLink'
import { LayoutContext } from './layout.context'

export default function NavbarHome({ home, navbar={} }) {
  const { countryData, questionsDef } = navbar
  const { t, getLocaledUrl, locale } = useTranslation()
  const { isNavbarOpen, isScrollUnderOffset, setIsNavbarOpen, setScrollOnInit } = useContext(LayoutContext)

  const router = useRouter()
  const isCountrySection = router.pathname.indexOf('/country/') > 0

  const menuItems = (
    <div className="px-4 font-semibold">
      {home ? (
        <LinkScroll to="header" smooth={true} offset={0} duration={800} href="" className={`${isCountrySection ? 'block mt-1 sm:inline sm:ml-2' : 'block mt-1'} active lg:inline lg:mt-0 menu-btn font-regular`}>
          {t('common.menu.home')}
        </LinkScroll>
      ) : (
        <ActiveLink href={`/${locale}`} activeClass="active" scroll={false}>
          <a onClick={() => setScrollOnInit('header')} className={`${isCountrySection ? 'block sm:inline' : 'block'} lg:inline lg:mt-0 menu-btn`}>
            {t('common.menu.home')}
          </a>
        </ActiveLink>
        )}
      {isCountrySection ? (
        <LinkScroll to="country-header" smooth={true} offset={0} duration={800} href="#country-header" activeClass="none" className={`${isCountrySection ? 'block mt-1 sm:inline sm:ml-2' : 'block mt-1'} active lg:inline lg:mt-0 lg:ml-2 menu-btn font-regular`}>
          {countryData.name}
        </LinkScroll>
      ) : ''}
      <ActiveLink href={`/${locale}/page/${getLocaledUrl('what-is')}`} scroll={false} activeClass="active">
        <a onClick={() => setScrollOnInit('intro')} className={`${isCountrySection ? 'block mt-1 sm:inline sm:ml-2' : 'block mt-1'} lg:inline lg:mt-0 lg:ml-2 menu-btn`}>
          {t('common.menu.what-is')}
        </a>
      </ActiveLink>
      <ActiveLink href={`https://www.alsur.lat/contacto`} scroll={false} activeClass="active">
        <a className={`${isCountrySection ? 'block mt-1 sm:inline sm:ml-2' : 'block mt-1'} lg:inline lg:mt-0 lg:ml-2 menu-btn`}>
          {t('common.menu.contacts')}
        </a>
      </ActiveLink>
      <div className={`${isCountrySection ? 'block mt-1 sm:inline sm:ml-2' : 'block'} mt-2 lg:inline lg:mt-0 lg:ml-2`}>
        <LocaleSwitcher />
      </div>
    </div>
  )

  return (
    <>
      <header className={`z-30 sm:flex sm:h-14 sm:px-4 sm:py-2 w-full fixed top-0 left-0 ${(isNavbarOpen || isScrollUnderOffset) ? 'shadow-lg bg-white' : ''}`} style={{maxHeight: '46px'}}>
        <div className="flex w-full px-4 py-2 sm:p-0 place-content-between place-items-center" style={{maxHeight: '46px'}}>
          {/* navbar header */}
          <div className={`${(isNavbarOpen || isScrollUnderOffset) ? 'transition-opacity-full' : 'transition-opacity-none'}`}>
            <Link href={`/${locale}`} scroll={false}>
              <a onClick={() => setScrollOnInit('header')} className="flex flex-row mx-auto place-items-center" title={t('common.al-sur')}>
                <div className="flex w-5 sm:w-7">
                  <img src="/svg/logo-img.svg" />
                </div>
                <div className="pl-2 pt-0.5 text-lg font-medium text-brandDarkBlue">
                  {t('common.observatory')}
                  <span className="pl-1 uppercase">
                    {t('common.covid-19')}
                  </span>
                </div>
                <div className="pl-1 pt-0.5 flex">
                  <img className="h-3.5" src="/svg/logo-text-blue.svg" />
                </div>
              </a>
            </Link>
          </div>
          <div className="lg:hidden">
            <button onClick={() => setIsNavbarOpen(!isNavbarOpen)} className="block w-5 pl-1 text-center text-gray-500 focus:text-brandBlue focus:outline-none" type="button">
              <div className="w-1.5 mx-auto h-auto sm:w-2">
                {isNavbarOpen && <img src="/svg/icon-menu-hover.svg" />}
                {!isNavbarOpen && <img src="/svg/icon-menu.svg" />}
              </div>
            </button>
          </div>
          {/* navbar content desktop */}
          <div className={`hidden lg:flex bg-white p-1.5 rounded-full ${(!isScrollUnderOffset && !isNavbarOpen) ? 'shadow-lg' : ''}`}>
            {menuItems}
          </div>
        </div>
      </header>
      {/* navbar content mobile */}
      <div className={`z-20 fixed top-0 left-0 w-4/5 sm:w-3/4 md:w-3/5 h-screen bg-white overflow-y-scroll pt-16 pb-4 sm:pt-20 lg:hidden ${isNavbarOpen ? 'block visible' : 'hidden invisible'}`}>
        <div className="mb-6">
          {menuItems}
        </div>
        {questionsDef && <>
          <QuestionsIndex countryData={countryData} questionsDef={questionsDef}></QuestionsIndex>
        </>}
      </div>
      <style jsx>{`
      `}</style>
    </>
  )
}
