import useTranslation from '../lib/translations/useTranslation.hook'
import LocaleSwitcher from './localeSwitcher'


export default function Intro ({ showIntroSet }) {
  const { t, locale } = useTranslation()
  return (
    <>
      <div className="w-full h-full absolute top-0 left-0 bg-white overflow-hidden">
        <iframe src={`https://tedicpy.github.io/covidAlSur/index.html?lang=${locale}`} className="w-full" style={{height: '100vh'}}></iframe>
        <div style={{height: '100px'}} className="fixed bottom-0 text-center p-15 w-full text-xs sm:text-base">
          <button className="scrolly-btn bg-brandRed text-white rounded-l-full mr-1 px-5 py-2 mt-5 inline-block" onClick={() => showIntroSet(false)}>
            {t('common.close-intro')}
          </button>
          <div className="scrolly-btn bg-brandDarkBlue text-white rounded-r-full px-5 py-2 mt-5 inline-block">
            <LocaleSwitcher selectClassName="bg-brandDarkBlue text-white" optionClassName="bg-white text-brandDarkBlue" />
          </div>
        </div>
      </div>
      <style jsx>{`
        .scrolly-btn {
          box-shadow: 0 6px 11px 4px #fff;
        }
      `}</style>
    </>
  );
}
