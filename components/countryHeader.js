import React from 'react'
import { motion } from "framer-motion"
import useTranslation from '../lib/translations/useTranslation.hook'
import InitiativeSelect from './initiativeSelect'

export default function CountryHeader({ countryData }) {
  const { t } = useTranslation()
  
  return (
    <section>
      <a id="country-header" className="block p-4 py-24 -my-10 xl:py-32">
        <motion.div layoutId={countryData.keyName} className="flex items-center mx-auto w-full place-content-center xl:w-2/3">
          <div>
            <motion.img className="block shadow-xl w-36 lg:w-40 xl:w-48 2xl:w-52 rounded-2xl" src={`/flags-icons/4x3/${countryData.code}.svg`} alt={`${t('common.flag-of')} ${countryData.name}`} />
          </div>
          <motion.div layout="position" className="pl-4">
            <h5 className="pb-0 text-sm uppercase lg:text-base xl:text-lg 2xl:text-xl">
              {t('country.country')}
            </h5>
            <h2 className="pt-0 text-xl font-extrabold uppercase lg:text-2xl xl:text-3xl 2xl:text-4xl">
              {countryData.name}
            </h2>
          </motion.div>
        </motion.div>

        <div className="px-0 pt-5 pb-0 -mb-5 -mx-4 sm:hidden">
          <InitiativeSelect countryData={countryData} design="block"></InitiativeSelect>
        </div>

        <div className="px-12 pt-7 pb-0 -mb-5 mx-auto hidden sm:block">
          <InitiativeSelect countryData={countryData} design="inline"></InitiativeSelect>
        </div>
      </a>
    </section>
  )
}