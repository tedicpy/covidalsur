import { useContext } from 'react'
import Link from 'next/link'
import useTranslation from '../lib/translations/useTranslation.hook'
import { LayoutContext } from './layout.context'


export default function Footer({ footerRef }) {
  const { t, getLocaledUrl, locale } = useTranslation()
  const { setScrollOnInit } = useContext(LayoutContext)

  return (
    <footer className="flex flex-col" ref={footerRef}>
      <div className="w-full overflow-x-hidden">
        <div className="h-10 -mx-20 overflow-hidden sm:-mx-4 sm:h-12 md:-mx-2 md:h-14 lg:-mx-0 lg:h-16 xl:h-20">
          <img className="place-items-center" src="/svg/dot-block.svg" />
        </div>
      </div>

      <div className="flex flex-col px-12 py-10 text-white bg-brandDarkBlue sm:flex-row place-items-center sm:place-content-between">
        <Link href="https://www.alsur.lat/" target="blank">
          <a className="flex flex-row mb-5 sm:mb-0" title={t('common.al-sur')}>
            <div className="flex w-10">
              <img src="/svg/logo-img.svg" />
            </div>
            <div className="flex w-24 pl-2">
              <img src="/svg/logo-text-white.svg" />
            </div>
          </a>
        </Link>

        <div className="flex flex-col font-light tracking-wide text-center sm:text-right">
          <Link href="https://www.tedic.org/" target="blank">
            <a className="flex flex-col mb-5 sm:mb-0" title={t('common.footer.logo')}>
              <div>
                {t('common.footer.by')}
              </div>
              <div className="w-14">
                <img src="/svg/tedic_logo.svg" />
              </div>
            </a>
          </Link>
        </div>

        <div className="flex flex-col font-light tracking-wide text-center sm:text-right">
          <Link href={`/${locale}/page/${getLocaledUrl('terms-and-conditions')}`} scroll={false}>
            <a onClick={() => setScrollOnInit('content')}>{t('common.pages.terms-and-conditions')}</a>
          </Link>
          <Link href={`/${locale}/page/${getLocaledUrl('privacy-policy')}`} scroll={false}>
            <a onClick={() => setScrollOnInit('content')}>{t('common.pages.privacy-policy')}</a>
          </Link>
          <Link href={`https://www.alsur.lat/contacto`} scroll={false}>
            <a>{t('common.pages.contacts')}</a>
          </Link>
        </div>
      </div>
    </footer>
  )
}
