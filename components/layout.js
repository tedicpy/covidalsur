import { useContext } from 'react'
import Head from 'next/head'
import Navbar from './navbar'
import useTranslation from '../lib/translations/useTranslation.hook'
import { LayoutContext } from './layout.context'
import BackToTop from './backToTop'


export default function Layout({ children, home, title, navbar }) {
  const { t } = useTranslation()
  const pageTitle = title ? `${title} | ${t('common.site-title')}` : t('common.site-title')
  const { contentRef, isNavbarOpen, setIsNavbarOpen } = useContext(LayoutContext)

  return (
    <>
      <Head>
        <link rel="icon" href="/favicon.ico" />
        <title>{pageTitle}</title>
        <meta
          name="description"
          content="Observatorio Covid-19 Al Sur"
        />
        {/* <meta
          property="og:image"
          content={`https://og-image.now.sh/${encodeURI(
            t('common.site-title')
          )}.png?theme=light&md=0&fontSize=75px&images=https%3A%2F%2Fassets.zeit.co%2Fimage%2Fupload%2Ffront%2Fassets%2Fdesign%2Fnextjs-black-logo.svg`}
        /> */}
        <meta name="og:title" content={t('common.site-title')} />
        <meta name="twitter:card" content="summary_large_image" />
      </Head>
      <Navbar home={home} navbar={navbar}></Navbar>
      <button className={`fixed z-10 w-full h-screen bg-black opacity-50 lg:hidden ${isNavbarOpen ? 'transition-opacity-full' : 'transition-opacity-none'}`} onClick={() => {setIsNavbarOpen(false)}} type="button"></button>
      <BackToTop></BackToTop>     
      <main className="overflow-x-hidden w-sceen" ref={contentRef}>{children}</main>
    </>
  )
}