import React from 'react'
import Link from 'next/link'
import useTranslation from '../lib/translations/useTranslation.hook'
import config from '../config'

export default function downloadDataSource() {
  const { t } = useTranslation()
  return (
    <>
      <div className={`flex flex-row p-5 mx-5 bg-brandRed rounded-2xl place-content-center place-items-center md:max-w-md md:mx-auto `}>
        <div className="">
          <img className="w-20 ml-3" src="/svg/download-pale-pink.svg" alt="" />
        </div>
        <div className="ml-8">
          <Link href={`/${config.dataSourcesPrefix}.zip`} target="blank">
            <a className="leading-tight download-link">
              <span className="text-lg font-bold text-white uppercase border-brandRed">{t('common.download.0')}</span>
              <br />
              <span className="border-b-2 text-rose-200 border-brandRed">{t('common.download.1')}</span>
            </a>
          </Link>
        </div>
      </div>
      <style jsx>{`
        a.download-link span {
          border-bottom-width: 1px;
        }
        a.download-link:hover span {
          border-bottom: 1px solid white !important;
        }
      `}</style>
    </>
  )
}