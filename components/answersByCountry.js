import slugify from 'slugify'
import { useToasts } from 'react-toast-notifications'
import copy from 'copy-to-clipboard'
import DownloadDataSource from './downloadDataSource'
import useTranslation from '../lib/translations/useTranslation.hook'
import Answer from './answer'

export default function AnswersByCountry({ countryData, countryAnswers, questionsDef }) {
  const { t, locale } = useTranslation()
  const { addToast } = useToasts()
  
  const copyQuestionLink = (question) => {
    copy(window.location.origin + window.location.pathname + '#' + slugify(question, { lower: true }))
    addToast(t('country.link-copied'), { appearance: 'info' })
  }
  
  return (
    <>
      <section className="flex flex-col w-full pb-16 bg-brandPalePink">
        <h3 className="px-4 py-5 text-xl font-bold text-white uppercase bg-brandDarkBlue">
          {t('country.answers')}
        </h3>
        
        <div className="flex flex-col break-words">
          {questionsDef.map(({ question, dimension, dataType }, index) => (
            <div className="px-3 pt-20 sm:pt-20 sm:px-6 md:px-10 xl:px-12" key={question} id={slugify(question, { lower: true })}>
              <div className="p-3 bg-white pb-7 sm:p-4 sm:pb-9 md:p-6 md:pb-11 rounded-2xl">
                <div className="float-left pr-2 -mt-1 text-5xl font-bold">{index + 1}</div>
                <div className="pl-16">
                  <h5 className="text-xs leading-relaxed text-blue-700 uppercase">
                    {dimension}
                    <span className="hidden">{dataType}</span>
                  </h5>
                  <h4 className="pb-1 text-xl leading-tight cursor-pointer question text-brandDarkBlue">
                    <button className="text-left" onClick={() => copyQuestionLink(question)} type="button">
                      <span className="mr-2 font-bold leading-none question">
                        {question}
                      </span>
                      <span className="copy-link">
                        <img className="inline w-3" src="/svg/icon-link.svg" alt={t('country.copy-link')} />
                      </span>
                    </button>
                  </h4>
                </div>
                <div className="clear-both"></div>
              </div>

              <div className="p-4 -mt-6 font-normal leading-relaxed shadow-lg prose-md rounded-xl sm:p-4 md:p-6 text-md bg-pink-50 answer">
                <Answer content={countryAnswers[index]} dataType={dataType}></Answer>
              </div>
            </div>
          ))}
        </div>

        <div className="py-20 pt-28 sm:pt-32">
          <DownloadDataSource></DownloadDataSource>
        </div>
      </section>
      <style jsx>{`
        .copy-link {
          display: none;
        }
        h4.question:hover .copy-link {
          display: inline;
        }
        h4.question:hover .question {
          border-bottom: 2px dotted #0338d6; // brand-blue
        }
        .bottom-full{bottom: 100%}
        .top-full{top: 100%}
      `}</style>
    </>
  )
}