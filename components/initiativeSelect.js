import { useContext } from 'react'
import Link from 'next/link'
import useTranslation from '../lib/translations/useTranslation.hook'
import { LayoutContext } from './layout.context'

export default function InitiativeSelect({ countryData, design = 'block' }) {
  const { t, locale } = useTranslation()
  const { setScrollOnInit } = useContext(LayoutContext)
  let content
  if (design === 'block') {
    content = (
      <>
        <h3 className="px-4 pb-3 text-xl font-bold uppercase pt-7 text-brandDarkBlue">{t('country.initiatives')}</h3>
        <ul className="flex flex-col leading-tight items-top">
          {countryData && countryData.initiatives.map(({ initiativeName, initiativeSlug }) => (
            <li className="px-4 py-1.5" key={initiativeName}>
              <Link as={`/${locale}/country/${encodeURIComponent(countryData.keyName)}/${encodeURIComponent(initiativeSlug)}`} href={{
                  pathname: '/[lang]/country/[keyName]/[initiativeSlug]',
                  query: { keyName: countryData.keyName, initiativeSlug, lang: locale }
                }}
                activeClass="active" scroll={false}
              >
                <a onClick={() => setScrollOnInit('country-header')} className={`block sm:inline lg:inline lg:mt-0 menu-btn ${countryData.initiativeSlug === initiativeSlug ? 'active' : ''}`}>
                  {initiativeName}
                </a>
              </Link>
            </li>
          ))}
        </ul>
      </>
    )
  }
  else if (design === 'inline') {
    content = (
      <div className="flex flex-row place-content-center">
        <h3 className="flex px-4 py-1.5 text-xl font-bold uppercase text-brandDarkBlue">{t('country.initiatives')}</h3>
        <ul className="flex flex-row leading-tight items-top flex-wrap">
          {countryData && countryData.initiatives.map(({ initiativeName, initiativeSlug }) => (
            <li className="px-4 py-2" key={initiativeName}>
              <Link as={`/${locale}/country/${encodeURIComponent(countryData.keyName)}/${encodeURIComponent(initiativeSlug)}`} href={{
                  pathname: '/[lang]/country/[keyName]/[initiativeSlug]',
                  query: { keyName: countryData.keyName, initiativeSlug, lang: locale }
                }}
                activeClass="active" scroll={false}
              >
                <a onClick={() => setScrollOnInit('country-header')} className={`block sm:inline lg:inline lg:mt-0 menu-btn ${countryData.initiativeSlug === initiativeSlug ? 'active' : ''}`}>
                  {initiativeName}
                </a>
              </Link>
            </li>
          ))}
        </ul>
      </div>
    )
  }

  return content
}