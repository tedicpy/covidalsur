import React from 'react'
import Autolinker from 'autolinker'
import ReactMarkdown from 'react-markdown'
import ChartBar from './chartBar'
import ChartPie from './chartPie'
import useTranslation from '../lib/translations/useTranslation.hook'

export default function Answer({ content, dataType }) {
  // Types:
    // text
    // date
    // chart_pie_single_percentage
    // chart_bars_percentage
    // chart_pie_number
    // yes_no_text
  // \b(?<!\.)(?!0+(?:\.0+)?%)(?:\d|[1-9]\d|100)(?:(?<!100)\.\d+)?%/g // selecciona los número porcentuales (que utilizan punto para decimales)
  // (.+): // selecciona todo el texto antes de ":" excluyendo saltos de línea y los caracteres especiales *: (en el caso de "Fuente: https:" selecciona toda la cadena)
  
  const { t, locale } = useTranslation()
  const autolinker = new Autolinker({
    replaceFn: (match) => {
      const displayName = match.getAnchorHref().replace('mailto:', '')
      return `[${displayName}](${match.getAnchorHref()})`
    }
  });
  const getContent = () => {
    return autolinker.link(content)
  }
  const compactArray = (list) => {
    const compact = []
    list.forEach(item => {
      if (item) {
        compact.push(item)
      }
    })
    return compact
  }
  const cleanNumbers = (numberList) => {
    if (!numberList) { return [] }
    const cleanList = []
    numberList.forEach(num => {
      const valueString = Number.parseInt(num.replace(/\:/g, '').replace(/\./g, '').replace(/\,/g, '')) // exclude thousands and decimal separators
      const parsedNum = Number.parseFloat(valueString)
      if (parsedNum) {
        cleanList.push(parsedNum)
      }
    })
    return cleanList
  }
  
  // chart_pie_single_percentage
  if (dataType === 'chart_pie_single_percentage') {
    const numbers = content.replace(/\*/g, '').match(/:(.+)/g)
    const labels = content.replace(/\*/g, '').match(/(.+):/g)
    if (numbers) {
      const data = [{
        label: labels[0].replace(/\:/g, ''),
        value: Number.parseFloat( numbers[0].replace(/\:/g, '').replace(/\,/g, '.') ) // use . as decimal separator
      }]
      // rest
      data[1] = {
        label: t('country.remaining'),
        value: 100 - data[0].value
      }
      return (
        <>
          <div className="pt-5 -m-1 -mt-3 h-96 lg:h-64 lg:pt-0">
            <ChartPie data={data} type='percentage'></ChartPie>
          </div>
          <ReactMarkdown source={getContent()}></ReactMarkdown>
        </>
      )
    }
  }
  // chart_bars_percentage
  if (dataType === 'chart_bars_percentage') {
    const percentages = content.replace(/,/g, '.').match(/\b(?<!\.)(?!0+(?:\.0+)?%)(?:\d|[1-9]\d|100)(?:(?<!100)\.\d+)*%/g)
    const labels = content.replace(/\*/g, '').match(/(.+):/g)
    if (percentages) {
      const data = compactArray(percentages.map((percent, index) => {
        if (labels[index]) {
          return {
            label: labels[index].replace(/\:/g, ''),
            value: Number.parseFloat( percent.replace(/\%/g, '') )
          }
        }
      }).reverse())
      return (
        <>
          <div className="-m-1 -mt-3" style={{ height: (data.length * 50) }}>
            <ChartBar data={data} opts={{ label: (d => `${d.data.label}: ${d.value}%`) }}></ChartBar>
          </div>
          <ReactMarkdown source={getContent()}></ReactMarkdown>
        </>
      )
    }
    // else => text // default
  }
  // chart_pie_number
  if (dataType === 'chart_pie_number') {
    const numbers = cleanNumbers( content.replace(/\*/g, '').match(/:(.+)/g) )
    const labels = content.replace(/\*/g, '').match(/(.+):/g)
    if (numbers.length && labels.length) {
      const data = numbers.reduce((accumulator, num, index) => {
        if (num && labels[index]) {
          accumulator.push({
            label: labels[index].replace(/\:/g, ''),
            value: num
          })
        }
        return accumulator
      }, [])
      return ( 
        <>
          <div className="pt-5 -m-1 -mt-3 h-96 lg:h-64 lg:pt-0">
            <ChartPie data={data}></ChartPie>
          </div>
          <ReactMarkdown source={getContent()}></ReactMarkdown>
        </>
      )
    }
  }
  // yes_no_text
  if (dataType === 'yes_no_text') {
    let isYes = new RegExp(`(^${t('country.yes')})`, 'i').exec(content)
    if (!isYes) {
      isYes = new RegExp(`(^${t('country.yes-alt')})`, 'i').exec(content)
    }
    let isNo = new RegExp(`(^${t('country.no')})`, 'i').exec(content)
    let icon = isYes ? 'icon-yes' : (isNo ? 'icon-no' : 'icon-na')
    return (
      <>
        <img src={`/svg/${icon}.svg`} className="float-left w-7" />
        <ReactMarkdown className="pl-16" source={getContent()}></ReactMarkdown>
        <div className="clear-both"></div>
      </>
    )
  }
  // date
  if (dataType === 'date') {
    const options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' }
    const UTCDateString = content.replace('GMT', '') // ignore timezone
    return (
      <span>{new Date(UTCDateString).toLocaleDateString(locale, options)}</span>
    )
  }
  // text // default
  return (
    <ReactMarkdown source={getContent()}></ReactMarkdown>
  )
}